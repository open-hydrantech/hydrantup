import React from 'react'
import { I18nManager, InteractionManager } from 'react-native'
import RNRestart from 'react-native-restart'
import to from 'await-to-js'
import _ from 'lodash'
import { reaction, when, toJS, useStrict, observable, action, computed } from 'mobx'

import Settings from '../config/settings'
import * as u from '../lib/Utils'

useStrict(true)

class Store {
	@action s = s => _.assign(this, s)
	@observable lastComm
	@observable connected = true
	@observable user
	@computed
	get auth() {
		return this.user && u.isNotEmptyString(this.user.userId)
	}

	async init() {
		console.log('starting')
		u.Server.connect()
		this.autoConnect()

		reaction(
			() => this.auth,
			async auth => {
				if (auth) this.autoConnect('start')
				else this.autoConnect('stop')
			},
		)

		const user = await u.LocalStorage.get('user')
		console.log('user', user)
		this.s({ user })
	}
	constructor(root) {
		this.root = root
		this.init()
	}

	@action
	logout() {
		console.log('logout')
		const { user } = this
		u.toto({
			timeOut: 15000,
			promise: u.meteorCall('mobile.logout', { user, appName: Settings.APP_NAME }),
		})
		this.s({ user: undefined })
		u.LocalStorage.delete('user')
	}

	@action
	async login({ email, password }) {
		console.log('login')

		const [err] = await to(u.meteorLoginWithPassword(email, password))
		console.log('1')
		if (err) return 'אימייל או סיסמא אינם מתאימים'
		console.log('2')

		const { data } = await u.toto({
			timeOut: 15000,
			promise: u.meteorCall('mobile.login', {
				deviceInfo: u.getDeviceInfo(),
				appName: Settings.APP_NAME,
			}),
		})
		const user = _.get(data, 'user')
		console.log('user', user)
		if (!user) return 'אימייל או סיסמא אינם מתאימים'

		this.s({ user })
		await u.LocalStorage.save('user', this.user)

		return true
	}

	@action
	setFcmToken({ fcmToken }) {
		console.log('sync', 'fcmToken', fcmToken)
		this.fcmToken = fcmToken
		const { user } = this
		u.toto({
			timeOut: 15000,
			promise: u.meteorCall('mobile.set.fcmtoken', { user, fcmToken, appName: Settings.APP_NAME }),
		})
	}

	@action
	async autoConnect(event) {
		console.log('connect')
		if (event == 'start') this.running = true
		else if (event == 'stop') {
			this.running = false
			return
		} else if (!this.running) return
		const enterDate = u.now()

		const { data } = await u.toto({
			timeOut: 25000,
			promise: u.meteorCall('mobile.connect'),
		})
		if (_.get(data, 'status') == 'OK') {
			this.s({ connected: true, lastComm: u.now() })
		} else {
			if (this.timeOut) {
				this.s({ connected: false })
				console.log('resetting connection')
				await u.Server.reset()
				this.timeOut = false
			}
			this.timeOut = true
		}

		const spent = u.now() - enterDate
		setTimeout(() => this.autoConnect(), spent > 10000 ? 1000 : 10000 - spent)
	}
}

export default Store
